import pytest
from src.hello import greeting

def test_hello():
    assert greeting() == ("Hello world!")
