from flask import Flask
app = Flask(__name__)


@app.route("/")
def greeting() -> str:
    """Greets the user with a nice message
    
    Returns:
        the string "Hello world"
    """

    return "Hello world!"


if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0')
